package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

    //test
    private final int a = 5;
    private final int b = 8;
    private int s;

    @GetMapping("/")
    public String home() {
        return "Spring is here!";
    }

    @GetMapping("area")
    public Integer area() {
        s = a * b;
        return s;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
